<?php
include('head-main.php');
include('nav-main.php');
?>
<!-- Hero -->
<section id="slider" class="hero p-0 odd">
    <div class="swiper-container full-slider animation slider-h-100 slider-h-auto">
        <div class="swiper-wrapper">

            <!-- Item 1 -->
            <div class="swiper-slide slide-center">

                <!-- Media -->
                <img src="./assets/images/cover/coverone.jpg" alt="Full Image" class="full-image" data-mask="70">

                <div class="slide-content row">
                    <div class="col-12 d-flex justify-content-start inner">
                        <div class="left text-left">

                            <!-- Content -->
                            <h1 data-aos="zoom-in" data-aos-delay="2000" class="title effect-static-text" style="word-spacing: 10px;">
                            FROM CONCEPT TO CREATION</span></span>
                            </h1>
                            <p data-aos="zoom-in" data-aos-delay="2400" class="description">Our projects have been built to the highest standards. Consistently over 50 years the ability to be inventive while never compromising standards.</p>

                            <!-- Action -->
                            <div data-aos="fade-up" data-aos-delay="2800" class="buttons">
                                <div class="d-sm-inline-flex">
                                    <a href="#contact" class="smooth-anchor mt-4 btn primary-button">GET IN TOUCH</a>
                                    <a href="#projects" class="smooth-anchor ml-sm-4 mt-4 btn outline-button">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Item 2 -->
            <div class="swiper-slide slide-center">

                <!-- Media -->
                <img src="./assets/images/cover/covertwo.jpg" alt="Full Image" class="full-image" data-mask="70">

                <div class="slide-content row">
                    <div class="col-12 d-flex justify-content-start justify-content-md-center inner">
                        <div class="center text-left text-md-center">

                            <!-- Content -->
                            <h2 data-aos="zoom-in" data-aos-delay="400" class="title effect-static-text" style="word-spacing: 10px;">
                            AN EXCEPTIONAL EXPERIENCE EVERY TIME</h2>
                            <p data-aos="zoom-in" data-aos-delay="800" class="description mr-auto ml-auto">The depth of construction expertise and outreach across territories enables us roll out projects in quick succession.</p>

                            <!-- Action -->
                            <div data-aos="fade-up" data-aos-delay="1200" class="buttons">
                                <div class="d-sm-inline-flex">
                                    <a href="#contact" class="smooth-anchor mt-4 btn primary-button">GET IN TOUCH</a>
                                    <a href="#projects" class="smooth-anchor ml-sm-4 mt-4 btn outline-button">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Item 3 -->
            <div class="swiper-slide slide-center">

                <!-- Media -->
                <img src="./assets/images/cover/coverthree.jpg" alt="Full Image" class="full-image" data-mask="70">

                <div class="slide-content row">
                    <div class="col-12 d-flex justify-content-start justify-content-md-center inner">
                        <div class="center text-left text-md-center">

                            <!-- Content -->
                            <h2 data-aos="zoom-in" data-aos-delay="400" class="title effect-static-text" style="word-spacing: 10px;">
                            WHERE TIMELESS MEETS TIMELY</h2>
                            <p data-aos="zoom-in" data-aos-delay="800" class="description mr-auto ml-auto">With technical expertise in every aspect of front end and back of the house services and deep knowledge of food and beverage as well as retail hospitality is a natural progression</p>

                            <!-- Action -->
                            <div data-aos="fade-up" data-aos-delay="1200" class="buttons">
                                <div class="d-sm-inline-flex">
                                    <a href="#contact" class="smooth-anchor mt-4 btn primary-button">GET IN TOUCH</a>
                                    <a href="#projects" class="smooth-anchor ml-sm-4 mt-4 btn outline-button">READ MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>



<!-- Fun Facts -->
<section id="counter" class="section-2 highlights  counter funfacts featured">

    <div class="row">
        <div class="col-12 col-md-5 pl-2 image">
            <img src="assets/images/aboutcover.jpg" class="fit-image" alt="Fit Image" style="height: 300px;">
        </div>
        <div class="col-12 col-md-7 align-self-center text-center text items">

            <div class="row items">
                <div data-aos="fade-up" class="col-12 col-md-3 p-3 item">
                    <div data-percent="25" class="radial left">
                        <span></span>
                    </div>
                    <p class="countertitle"><strong>Years of project experiences</strong></p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-3 p-3 item">
                    <div data-percent="1600" class="radial left">
                        <span></span>
                    </div>
                    <p class="countertitle"><strong>Projects Done</strong></p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-3 p-3 item">
                    <div data-percent="517" class="radial left">
                        <span></span>
                    </div>
                    <p class="countertitle"><strong>Architects and Engineers</strong></p>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-3 p-3 item">
                    <div data-percent="4000" class="radial left">
                        <span></span>
                    </div>
                    <p class="countertitle"><strong>Happy Experiences</strong></p>
                </div>
            </div>

            <!-- Action -->
            <div data-aos="fade-up" class="buttons">
                <div class="d-sm-inline-flex mb-5 mb-md-0">
                    <a href="/aboutus.php" class=" mx-auto mt-4 btn primary-button">ABOUT US</a>
                    <a href="#contact" class="mx-auto ml-sm-4 mt-4 btn outline-button">CONTACT US</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="aboutus" class="section-2 highlights  counter funfacts featured">

    <div class="row">
        <div class="col-12 col-md-12 align-self-center  text items">
            <div class="row intro">
                <div class="col-12 p-0">
                    <h2 style=" color: #fdbb1a; ">About us</h2>
                    <p>CODE has high quality capabilities in Civil and Interior as well as Engineering Services. Projects of Residential, Commercial, Institutional, Corporate, Hospitality and Industrial buildings.</p>
                    <div class="text-left" style="padding-left:2%">
                        <p class="about-list"><i class="icon-arrow-right"></i>Experience of team over 20 years. Set to reach a 50 Cr turnover in 2020</p>
                        <p class="about-list"><i class="icon-arrow-right"></i>Has a committed project order portfolio.</p>
                        <p class="about-list"><i class="icon-arrow-right"></i>Known for ethical techno commercial practices and timely completion.</p>
                        <p class="about-list"><i class="icon-arrow-right"></i>Turnkey capabilities and in-house expertise in manufacturing of product in a facility of a factory.</p>
                    </div>
                    <p>Our expertise spans an extensive range of services with Civil engineering excellence, design-build projects and construction management At Code Constructions the workforce is always driven to deliver projects promptly on-time using value driven solutions and top-notch project management techniques. We focus on supporting large-scale projects both private and public initiatives.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Projects -->
<section id="projects" class="section-3 odd showcase blog-grid filter-section projects">
    <div class="overflow-holder">
        <div class="container">
            <div class="row text-center intro">
                <div class="col-12">
                    <span class="pre-title">We do more for everyone</span>
                    <h2 class="mb-0">Our Projects
                    </h2>
                </div>
            </div>
            <div class="row justify-content-center text-center">
                <div class="col-12">
                    <div class="btn-group" data-toggle="buttons">
                        <a href="/corporate.php" target="_blank" class="btn">Corporate</a>
                        <a href="/residential.php" target="_blank" class="btn">Residential</a>
                        <a href="/hospitality.php" target="_blank" class="btn">Hospitality</a>
                        <a href="/housing.php" target="_blank" class="btn">Housing</a>
                    </div>
                </div>
            </div>
            <div class="row items">
                <div class="col-12 col-md-6 col-lg-3 mb-3">
                    <div class="row card p-0 text-center">
                        <div class="image-over">
                            <img src="assets/images/articlecorp.jpg" alt="Lorem ipsum">
                        </div>
                        <div class="card-caption col-12 p-0">
                            <div class="card-body">
                                <a href="/corporate.php">
                                    <h4>Corporate</h4>
                                </a>
                            </div>
                        </div>
                        <a href="/corporate.php"><i class="btn-icon fas fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-3">
                    <div class="row card p-0 text-center">
                        <div class="image-over">
                            <img src="assets/images/articleresident.jpg" alt="Lorem ipsum">
                        </div>
                        <div class="card-caption col-12 p-0">
                            <div class="card-body">
                                <a href="/residential.php">
                                    <h4>Residential</h4>
                                </a>
                            </div>
                        </div>
                        <a href="/residential.php"><i class="btn-icon fas fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-3">
                    <div class="row card p-0 text-center">
                        <div class="image-over">
                            <img src="assets/images/articlehosp.jpg" alt="Lorem ipsum">
                        </div>
                        <div class="card-caption col-12 p-0">
                            <div class="card-body">
                                <a href="/hospitality.php">
                                    <h4>Hospitality
                                    </h4>
                                </a>
                            </div>
                        </div>
                        <a href="/hospitality.php"><i class="btn-icon fas fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3 mb-3">
                    <div class="row card p-0 text-center">
                        <div class="image-over">
                            <img src="assets/images/articlehousing.jpg" alt="Lorem ipsum">
                        </div>
                        <div class="card-caption col-12 p-0">
                            <div class="card-body">
                                <a href="/housing.php">
                                    <h4>Housing</h4>
                                </a>
                            </div>
                        </div>
                        <a href="/housing.php"><i class="btn-icon fas fas fa-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-1 filter-sizer"></div>
            </div>
        </div>
    </div>
</section>

<!-- Contact -->
<section id="contact" class="section-2 odd form contact featured">
    <div class="row">
        <div class="col-12 col-md-8 pr-md-5 align-self-center text">
            <div class="row intro contactintro">
                <div class="col-12 p-0">
                    <span class="pre-title m-0">Send a message</span>
                    <h2>Get in Touch</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-12 p-0">
                    <form action="php/form.php" id="nexgen-simple-form" class="nexgen-simple-form">
                        <input type="hidden" name="section" value="nexgen_form">

                        <input type="hidden" name="reCAPTCHA">
                        <!-- Remove this field if you want to disable recaptcha -->

                        <div class="row form-group-margin" style=" width: 75%; ">
                            <div class="col-12 col-md-6 m-0 p-2 input-group">
                                <input type="text" name="name" class="form-control field-name" placeholder="Name">
                            </div>
                            <div class="col-12 col-md-6 m-0 p-2 input-group">
                                <input type="email" name="email" class="form-control field-email" placeholder="Email">
                            </div>
                            <div class="col-12 col-md-6 m-0 p-2 input-group">
                                <input type="text" name="phone" class="form-control field-phone" placeholder="Phone">
                            </div>
                            <!-- <div class="col-12 col-md-6 m-0 p-2 input-group">
                                <i class="icon-arrow-down mr-3"></i>
                                <select name="info" class="form-control field-info">
                                    <option value="" selected disabled>Select Product</option>
                                    <option>Corporate</option>
                                    <option>Hospitality</option>
                                    <option>Analytics and M&A</option>
                                    <option>Housing</option>
                                    <option>Residential</option>
                                    <option>Other</option>
                                </select>
                            </div> -->
                            <div class="col-12 m-0 p-2 input-group">
                                <textarea name="message" class="form-control field-message" placeholder="Message"></textarea>
                            </div>
                            <div class="col-12 col-12 m-0 p-2 input-group">
                                <span class="form-alert"></span>
                            </div>
                            <div class="col-12 input-group m-0 p-2">
                                <a class="btn primary-button">SEND</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-4">
            <div class="row intro contactintro">
                <div class="col-12 p-0">
                    <span class="pre-title m-0 mb-2">Communication address</span>
                    <img src="code.png" alt="logo" style=" width: 180px; ">
                </div>
            </div>
            <div class="contacts">
                <p></p>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-phone-alt mr-2"></i>  +91 44 2434 2734
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-envelope mr-2"></i> admin@codeeng.net
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <i class="fas fa-map-marker-alt mr-2"></i><span>10, ARK Colony Rd, Vannia<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Teynampet,Lubdhi Colony,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Alwarpet, Chennai,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tamil Nadu 600018.</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="mt-2 btn outline-button view-map" data-toggle="modal" data-target="#map">VIEW MAP</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section id="maps" class="section-6 odd form contact">
    <div class="overflow-holder">
        <div class="row">
            <div class="map-responsive">
                <div class="mapouter">
                    <div class="gmap_canvas"><iframe width="100%" height="300" id="gmap_canvas" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3886.970684431683!2d80.25240551350149!3d13.037537990812625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5266356057197d%3A0x5a29e5a8438b71e3!2s10%2C%20ARK%20Colony%20Rd%2C%20Vannia%20Teynampet%2C%20Lubdhi%20Colony%2C%20Alwarpet%2C%20Chennai%2C%20Tamil%20Nadu%20600018!5e0!3m2!1sen!2sin!4v1632420850588!5m2!1sen!2sin" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Footer -->
<?php
include('footer-main.php');
?>