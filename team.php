<!DOCTYPE html>

<html lang="en">

<head>

    <!-- ==============================================
        Basic Page Needs
        =============================================== -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->

    <title>CODE - Corporate, Hospitality, Housing, Residential</title>

    <meta name="description" content="Corporate, Hospitality, Housing, Residential">
    <meta name="subject" content="Corporate, Hospitality, Housing, Residential">
    <meta name="author" content="Antony">

    <!-- ==============================================
        Favicons
        =============================================== -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- ==============================================
        Vendor Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/vendor/slider.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/vendor/icons.min.css">
    <link rel="stylesheet" href="assets/css/vendor/icons-fa.min.css">
    <link rel="stylesheet" href="assets/css/vendor/animation.min.css">
    <link rel="stylesheet" href="assets/css/vendor/gallery.min.css">
    <link rel="stylesheet" href="assets/css/vendor/cookie-notice.min.css">

    <!-- ==============================================
        Custom Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/default.css">

    <!-- ==============================================
        Theme Color
        =============================================== -->
    <meta name="theme-color" content="#21333e">

    <!-- ==============================================
        Theme Settings
        =============================================== -->
    <style>
        :root {
            --hero-bg-color: #080d10;
            --section-1-bg-color: #ffffff;
            --section-2-bg-color: #111117;
            --section-3-bg-color: #ffffff;
            --section-4-bg-color: #eef4ed;
        }

        .navbar-nav .primary-button {
            /* background-color: #efc054; */
        }

        .navbar {
            background-color: black;
        }

        .share-list .nav-link i {
            background: #efc054;
        }

        .outline-button {
            /* background:#efc054; */
        }

        .outline-button:hover {
            background: #a5a5a5;
        }

        .card img {
            border-radius: 25px;
        }

        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
        }

        .modal-body {
            width: 100%;
            display: block;
            align-items: normal;
        }

        .modal-body p {
            margin: 0;
        }

        .modal-body .subtitle p {
            font-weight: 600;
        }

        .pln {
            margin: 5px;
        }
        .card{
            height: 100%;
        }
        .cardparent{
            position: relative;
        }
        .readmorebtn{
            position: absolute;
  bottom: 0;
  left: 0;
        }
        .card img{
            height: 270px;
        }
    </style>

</head>

<body>
<?php
include('nav-main.php');
?>
    <!-- Preloader -->
    <div id="preloader" data-timeout="2000" class="odd preloader counter">
        <div data-aos="fade-up" data-aos-delay="500" class="row justify-content-center text-center items">
            <div data-percent="100" class="radial">
                <span></span>
            </div>
        </div>
    </div>



    <!-- Hero -->
    <section id="partners" class="section-1 offers testimonials" style="background-color: #a5a5a5;padding-top: 50px;">
        <div class="container" style="max-width: 100%;">
            <div class="row text-center intro">
                <div class="col-12">
                    <h2 class="mt-5"> <span class="featured"><span>Meet our Team</span></span></h2>
                </div>
            </div>
            <div class="row justify-content-center items">
                <div data-aos="fade-up" class="col-12 col-md-4 item aos-init aos-animate">
                    <div class="card">
                        <div class="col-12 cardparent">
                            <img src="./assets/images/ks_crop.jpg" class="center" width="300" height="70" alt="" style="height: 269px;">
                            <div class="col-12 align-self-center text-center">
                                <h4 class="modalhead">Krithika Subrahmanian</h4>
                                <p class="modalhead">Transform Design Pvt Ltd. Founder | Director</p>
                                <p class="modalhead">Sreshta Leisure Pvt Ltd. Cofounder | Director</p>
                                <div class="text-left">
                                    <p>Heads a dynamic group of brands in the arenas of architecture, design, construction and luxury hotels. With 25 year old strong businesses to her asset list, her new ventures are scalable investment propositions. She is a deeply rooted third generation entrepreneur and culture expert.</p>
                                </div>
                            </div>


                            <div class="row readmorebtn">
                            <div class="col-md-6">
                                    <!-- Action -->
                                    <div class="buttons">
                                        <div class="d-sm-inline-flex">
                                            <a href="#" class="mt-3 btn outline-button" data-toggle="modal" data-target="#kris">READ MORE</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <ul class="navbar-nav social share-list ml-auto">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
                                        </li>
                                    </ul>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-4 item aos-init aos-animate">
                    <div class="card cardparent">
                        <div class="col-12">
                            <img src="./assets/images/sumanth.png" class="center" width="300" height="70" alt="">
                            <div class="col-12 align-self-center text-center">
                                <h4 class="modalhead">Sumanth Subramanian</h4>
                                <p class="modalhead">Sumanth &amp; Co Managing Director</p>
                                <p class="modalhead">Sreshta Leisure Pvt Ltd. ,Cofounder</p>
                                <div class="text-left">
                                    <p>Heads the renowned ‘Sreshta’ and ‘Sreyas’ brand of buildings. With a huge residential footprint across Chennai for almost 5 decades, Sumanth & Co has had a steady growth over the years winning goodwill and reputation among the thousands of residents it houses.</p>
                                </div>
                            </div>
                            <div class="row readmorebtn">
                                <div class="col-md-6">
                                <div class="buttons">
                                <div class="d-sm-inline-flex">
                                    <a href="#" class="mt-3 btn outline-button" data-toggle="modal" data-target="#sumanth">READ MORE</a>
                                </div>
                            </div>
                                </div>
                                <div class="col-md-6">
                                <ul class="navbar-nav social share-list ml-auto">
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                                </div>
                            </div>
                            <!-- Action -->


                        </div>
                    </div>
                </div>
                <div data-aos="fade-up" class="col-12 col-md-4 item aos-init aos-animate">
                    <div class="card">
                        <div class="col-12 cardparent">
                            <img src="./assets/images/vivek.jpg" class="center" width="300" height="70" alt="">
                            <div class="col-12 align-self-center text-center">
                                <h4 class="modalhead">Vivek Anandhan. N.V</h4>
                                <p class="modalhead">Chief executive officer</p>
                                <div class="text-left">
                                    <p>Heading the design team of Architects and liaising with Engineers of various disciplines. Responsible for spearheading all projects on the Design front including investments, joint ventures, management Contract projects. Advise development.</p>
                                </div>
                            </div>
                            <div class="row readmorebtn">
                                <div class="col-md-6">
                                <div class="buttons">
                                <div class="d-sm-inline-flex">
                                    <a href="#" class="mt-3 btn outline-button" data-toggle="modal" data-target="#vivek">READ MORE</a>
                                </div>
                            </div>
                                </div>
                                <div class="col-md-6">
                                <ul class="navbar-nav social share-list ml-auto">
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                                </div>
                            </div>
                            <!-- Action -->


                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- Modal [responsive menu] -->
    <div id="menu" class="p-0 modal fade" role="dialog" aria-labelledby="menu" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout" role="document">
            <div class="modal-content full">
                <div class="modal-header" data-dismiss="modal">
                    <i class="icon-close fas fa-arrow-right"></i>
                </div>
                <div class="menu modal-body">
                    <div class="row w-100">
                        <div class="items p-0 col-12 text-center">
                            <!-- Append [navbar] -->
                        </div>
                        <div class="contacts p-0 col-12 text-center">
                            <!-- Append [navbar] -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- modal start  -->

    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="kris" tabindex="-1" role="dialog" aria-labelledby="krisTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title modalhead" id="exampleModalLongTitle">Krithika Subrahmanian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="subtitle mb-1">
                        <p class="modalhead">Transform Design Pvt Ltd. Founder | Director </p>
                        <p class="modalhead">Sreshta Leisure Pvt Ltd. Cofounder | Director</p>
                    </div>
                    <p>Heads a dynamic group of brands in the arenas of architecture, design, construction and luxury hotels. With 25 year old strong businesses to her asset list, her new ventures are scalable investment propositions. She is a deeply rooted third generation entrepreneur and culture expert.</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Education:</strong>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>B. Arch – Gold Medalist. School of Architecture.</p>
                    <p>&nbsp;&nbsp;&nbsp;Anna University</p>
                    <p> <i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Graduate School of Business- Stanford Seed</p>
                    <p>&nbsp;&nbsp;&nbsp;India – 2019</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Senior Executive Leadership Program India</p>
                    <p>&nbsp;&nbsp;&nbsp;2019 – Harvard Business School</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Professional Affliations:</strong>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Registered Architect with the council of architecture</p><p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Life Member of Association of Indian Institute of Architects</p><p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Life Member of FICCI Ladies Organization</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Serves as a Board member in the family group of companies.</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Awards & Achievements:</strong>
                    <p>Winner of the TATLER Travel Award 2020 for SVATMA the hospitality venture as the best spa. www.svatma.in Published Author- ‘The Next Big Thing Thanjavur – A book on the history and culture of Thanjavur.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="sumanth" tabindex="-1" role="dialog" aria-labelledby="sumanthTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title modalhead" id="exampleModalLongTitle">Sumanth Subramanian</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="subtitle mb-2">
                        <p class="modalhead">Sumanth & Co Managing Director</p>
                        <p class="modalhead">Sreshta Leisure Pvt Ltd. ,Cofounder</p>
                    </div>
                    <p>Heads the renowned ‘Sreshta’ and ‘Sreyas’ brand of buildings. With a huge residential footprint across Chennai for almost 5 decades, Sumanth & Co has had a steady growth over the years winning goodwill and reputation among the thousands of residents it houses.</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Education:</strong>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Bachelors in Science (Physics)</p>
                    <p>&nbsp;&nbsp;&nbsp;University of Madras</p><p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Masters in Business</p>
                    <p>&nbsp;&nbsp;&nbsp;Administration, University of Notre Dame, U. S. A</p><br>
                    <strong>Professional Affliations:</strong>
                    <p class="pln">&nbsp;</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Member of CREDAI Gymkhana Club</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Cosmopolitan Club Ootacamund Gymkhana Club</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Kodaikanal Club Gandhi Nagar Club</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Lawley Institute</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Awards & Achievements:</strong>
                    <p class="pln">&nbsp;</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Elected Treasurer of CREDAI Tamil Nadu for 2006 – 2009</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Chairman of the Madras Round Table 2002 - 2003</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="vivek" tabindex="-1" role="dialog" aria-labelledby="sumanthTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title modalhead" id="exampleModalLongTitle">Vivek Anandhan. N.V</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="subtitle mb-2">
                        <p class="modalhead">Chief executive officer</p>
                    </div>
                    <p>Heading the design team of Architects and liaising with Engineers of various disciplines. Responsible for spearheading all projects on the Design front including investments, joint ventures, management Contract projects. Advise development.</p>
                    <p class="pln">&nbsp;</p>
                    <p>Responsible for all aspects of project programming, architectural design, mechanical and electrical drawing coordination, construction documents and construction supervision Was also responsible for preparation of construction documents and specifications for the projects. Develop presentation drawings and coordinate drawings with other disciplines. Applied artistic expertise and had the opportunity to work on a variety of projects including educational, municipal, professional office, institutional, commercial, religious, recreational, and residential. Responsible for site design, consultant coordination, in house coordination with the design team etc.</p>
                    <p class="pln">&nbsp;</p>
                    <strong>Awards & Achievements:</strong>
                    <p class="pln">&nbsp;</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Architect –Planner with over 20 years’ experience in planning, designing, detailing and coordinating with multi disciplinary teams and execution of projects.</p>
                    <p class="pln">&nbsp;</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Employee of the Year. Mouchel 2007, 2008</p>
                    <p class="pln">&nbsp;</p>
                    <p><i class="icon-arrow-right modalhead" style=" font-size: 5px; "></i>Won ISHA building design, Bangalore, India</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Scroll [to top] -->
    <div id="scroll-to-top" class="scroll-to-top">
        <a href="#header" class="smooth-anchor">
            <i class="fas fa-arrow-up"></i>
        </a>
    </div>

        <!-- Footer -->
        <?php
include('footer-main.php');
?>