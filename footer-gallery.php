    <!-- Footer -->
    <footer>

        <!-- Footer [content] -->
        <section id="footer" class="odd footer offers">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-3 footer-left">

                        <!-- Navbar Brand-->
                        <a class="navbar-brand" href="/">
                            <span class="brand">
                                <a class="navbar-brand" href="/">
                                    <img src="codewhite.png" alt="" style=" width: 160px; height: 46px; ">
                                </a>
                            </span>

                            <!-- 
                                    Custom Logo
                                    <img src="assets/images/logo.svg" alt="NEXGEN">
                                -->
                        </a>
                        <p><strong>CODE</strong> has high quality capabilities in Civil and Interior as well as Engineering Services. Projects of Residential , Commercial , Institutional , Corporate , Hospitality and Industrial buildings</p>
                        <p>© 2021 CODE</p>
                    </div>
                    <div class="col-12 col-lg-9 p-0 footer-right">
                        <div class="row items">
                            <div class="col-12 col-lg-4 item">
                                <div class="card">
                                    <h4>Our Projects</h4>
                                    <a href="#"><i class="icon-arrow-right"></i>Corporates</a>
                                    <a href="#"><i class="icon-arrow-right"></i>Residential</a>
                                    <a href="#"><i class="icon-arrow-right"></i>Hospitality</a>
                                    <a href="#"><i class="icon-arrow-right"></i>Housing</a>
                                </div>
                            </div>
                            <div class="col-12 col-lg-8 item">
                                <div class="card">
                                    <ul class="navbar-nav">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fas fa-phone-alt mr-2"></i> +91 (999) 888-1234
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fas fa-envelope mr-2"></i> admin@codeeng.net
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="fas fa-map-marker-alt mr-2"></i>10, ARK Colony Rd, Vannia Teynampet, Lubdhi Colony, Alwarpet, Chennai, Tamil Nadu 600018.
                                            </a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#contact" class="mt-4 btn outline-button smooth-anchor">GET IN TOUCH</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </footer>



    <!-- Modal [responsive menu] -->
    <div id="menu" class="p-0 modal fade" role="dialog" aria-labelledby="menu" aria-hidden="true">
        <div class="modal-dialog modal-dialog-slideout" role="document">
            <div class="modal-content full">
                <div class="modal-header" data-dismiss="modal">
                    <i class="icon-close fas fa-arrow-right"></i>
                </div>
                <div class="menu modal-body">
                    <div class="row w-100">
                        <div class="items p-0 col-12 text-center">
                            <!-- Append [navbar] -->
                        </div>
                        <div class="contacts p-0 col-12 text-center">
                            <!-- Append [navbar] -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scroll [to top] -->
    <div id="scroll-to-top" class="scroll-to-top">
        <a href="#header" class="smooth-anchor">
            <i class="fas fa-arrow-up"></i>
        </a>
    </div>

    <!-- ==============================================
        Google reCAPTCHA // Put your site key here
        =============================================== -->
    <script src="https://www.google.com/recaptcha/api.js?render=6Lf-NwEVAAAAAPo_wwOYxFW18D9_EKvwxJxeyUx7"></script>

    <!-- ==============================================
        Vendor Scripts
        =============================================== -->
    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="assets/js/vendor/jquery.easing.min.js"></script>
    <script src="assets/js/vendor/jquery.inview.min.js"></script>
    <script src="assets/js/vendor/popper.min.js"></script>
    <script src="assets/js/vendor/bootstrap.min.js"></script>
    <script src="assets/js/vendor/ponyfill.min.js"></script>
    <script src="assets/js/vendor/slider.min.js"></script>
    <script src="assets/js/vendor/animation.min.js"></script>
    <script src="assets/js/vendor/progress-radial.min.js"></script>
    <script src="assets/js/vendor/bricklayer.min.js"></script>
    <script src="assets/js/vendor/gallery.min.js"></script>
    <script src="assets/js/vendor/shuffle.min.js"></script>
    <script src="assets/js/vendor/cookie-notice.min.js"></script>
    <script src="assets/js/vendor/particles.min.js"></script>
    <script src="assets/js/main.js"></script>

</body>
</head>