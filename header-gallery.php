<!DOCTYPE html>

<html lang="en">

<head>

    <!-- ==============================================
        Basic Page Needs
        =============================================== -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
<?php $titles = array(
    'hospitality.php'=>'HOSPITALITY&nbsp;&nbsp;&nbsp;PORTFOLIO',
    'corporate.php'=>'CORPORATE&nbsp;&nbsp;&nbsp;PORTFOLIO',
    'housing.php'=>'HOUSING&nbsp;&nbsp;&nbsp;PORTFOLIO',
    'residential.php'=>'RESIDENTIAL&nbsp;&nbsp;&nbsp;PORTFOLIO'
    );
    $totlaImages = array(
        'hospitality.php'=>51,
        'corporate.php'=>39,
        'housing.php'=>23,
        'residential.php'=>44
        );
    $folderNames = array(
        'hospitality.php'=>'hospcomp',
        'corporate.php'=>'corpcomp',
        'housing.php'=>'houscomp',
        'residential.php'=>'resicomp'
    );
    $gallary =  basename($_SERVER['REQUEST_URI']);
    $industry = explode(".php",basename($_SERVER['REQUEST_URI']))[0];
    $title = $titles[$gallary];
    $imagecounter = $totlaImages[$gallary];
    $foldername = $folderNames[$gallary];
    
    ?>
    <title><?= $title ?></title>

    <meta name="description" content="<?= $title ?>">
    <meta name="subject" content="<?= $title ?>">
    <meta name="author" content="Antony">

    <!-- ==============================================
        Favicons
        =============================================== -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- ==============================================
        Vendor Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/vendor/slider.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/vendor/icons.min.css">
    <link rel="stylesheet" href="assets/css/vendor/icons-fa.min.css">
    <link rel="stylesheet" href="assets/css/vendor/animation.min.css">
    <link rel="stylesheet" href="assets/css/vendor/gallery.min.css">
    <link rel="stylesheet" href="assets/css/vendor/cookie-notice.min.css">

    <!-- ==============================================
        Custom Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/default.css">

    <!-- ==============================================
        Theme Color
        =============================================== -->
    <meta name="theme-color" content="#21333e">

    <!-- ==============================================
        Theme Settings
        =============================================== -->
    <style>
         :root {
            --hero-bg-color: #080d10;
            --section-1-bg-color: #ffffff;
            --section-2-bg-color: #111117;
            --section-3-bg-color: #ffffff;
            --section-4-bg-color: #eef4ed;
        }
        
        .navbar-nav .primary-button {
            background-color: #fdbb1a;
        }
        
        .about-list i {
            color: #fdbb1a;
        }
        .service-card{
            font-size:20px
        }
        section.section-2 {
    background-color:  #474747;}
    .effect-static-text{
        font-size: 35px!important;
    }
    .slider-h-auto.no-slider .inner{
        padding:0!important;
    }
    .slider-h-auto .swiper-wrapper, .slider-h-auto .swiper-slide { min-height: 10px; }
   
    </style>

</head>
<body>
    <!-- Preloader -->
    <div id="preloader" data-timeout="2000" class="odd preloader counter">
        <div data-aos="fade-up" data-aos-delay="500" class="row justify-content-center text-center items">
            <div data-percent="100" class="radial">
                <span></span>
            </div>
        </div>
    </div>

    <!-- Header -->
    <header id="header">

        <!-- Top Navbar -->
        <nav class="navbar navbar-expand top">
            <div class="container header">

                <!-- Nav holder -->
                <div class="ml-auto"></div>

                <!-- Navbar Items [right] -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-phone-alt mr-2"></i>+91 44 2434 2734</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-envelope mr-2"></i>admin@codeeng.net</a>
                    </li>
                </ul>

                <!-- Navbar Icons -->
                <ul class="navbar-nav icons">
                    <li class="nav-item social">
                        <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a href="#" class="nav-link pr-0"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                </ul>

            </div>
        </nav>

        <!-- Navbar -->
        <nav class="navbar navbar-expand navbar-fixed sub">
            <div class="container header">

                <!-- Navbar Brand-->
                <a class="navbar-brand" href="/">
                    <img src="codewhite.png" alt="logo" style=" width: 160px; height: 46px; ">
                </a>

                <!-- Nav holder -->
                <div class="ml-auto"></div>

                <!-- Navbar Items -->
                <ul class="navbar-nav items">
                    <li class="nav-item">
                        <a href="/" class=" nav-link">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a href="/aboutus.php" class=" nav-link">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a href="/team.php" class=" nav-link">OUR TEAM</a>
                    </li>
                    <li class="nav-item">
                        <a href="/#projects" class=" nav-link">OUR PROJECTS</a>
                    </li>
                </ul>

                <!-- Navbar Icons -->

                <!-- Navbar Toggle -->
                <ul class="navbar-nav toggle">
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#menu">
                            <i class="icon-menu m-0"></i>
                        </a>
                    </li>
                </ul>

                <!-- Navbar Action -->
                <ul class="navbar-nav action">
                    <li class="nav-item ml-3">
                        <a href="/#contact" class="smooth-anchor btn ml-lg-auto primary-button">CONTACT US</a>
                    </li>
                </ul>
            </div>
        </nav>

    </header>

    <!-- Hero -->
    <section id="slider" class="hero p-0 odd featured">
        <div class="swiper-container no-slider animation slider-h-50 slider-h-auto">
            <div class="swiper-wrapper">

                <!-- Item 1 -->
                <div class="swiper-slide slide-center">

                    <!-- Media -->
                    <img src="<?= 'assets/images/cover/'.$industry.'galcover.jpg' ?>" alt="Full Image" class="full-image" data-mask="80">

                    <div class="slide-content row text-center">
                        <div class="col-12 mx-auto inner">

                            <!-- Content -->
                            <h1 class="mb-0 title effect-static-text"><?= $title ?></h1>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>