<!DOCTYPE html>

<html lang="en">

<head>

    <!-- ==============================================
        Basic Page Needs
        =============================================== -->
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->

    <title>CODE - Corporate, Hospitality, Housing, Residential</title>

    <meta name="description" content="Corporate, Hospitality, Housing, Residential">
    <meta name="subject" content="Corporate, Hospitality, Housing, Residential">
    <meta name="author" content="Antony">

    <!-- ==============================================
        Favicons
        =============================================== -->
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/images/apple-touch-icon-114x114.png">

    <!-- ==============================================
        Vendor Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/vendor/slider.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/vendor/icons.min.css">
    <link rel="stylesheet" href="assets/css/vendor/icons-fa.min.css">
    <link rel="stylesheet" href="assets/css/vendor/animation.min.css">
    <link rel="stylesheet" href="assets/css/vendor/gallery.min.css">
    <link rel="stylesheet" href="assets/css/vendor/cookie-notice.min.css">

    <!-- ==============================================
        Custom Stylesheet
        =============================================== -->
    <link rel="stylesheet" href="assets/css/default.css">

    <!-- ==============================================
        Theme Color
        =============================================== -->
    <meta name="theme-color" content="#21333e">

    <!-- ==============================================
        Theme Settings
        =============================================== -->
    <style>
         :root {
            --hero-bg-color: #080d10;
            --section-1-bg-color: #ffffff;
            --section-2-bg-color: #111117;
            --section-3-bg-color: #ffffff;
            --section-4-bg-color: #eef4ed;
        }
        
        .navbar-nav .primary-button {
            background-color: #efc054;
        }
        
        .about-list i {
            color: #efc054;
        }
        .service-card{
            font-size:20px
        }
        section.section-4,section.section-1, body{
            background-color: #474747;
        }
        p{
            color:#ffff;
        }
        /* .card h4{
            color: #efc054;
        } */
        h2 .featured{
            padding:0;
        }
    </style>

</head>

<body>

    <!-- Preloader -->
    <div id="preloader" data-timeout="2000" class="odd preloader counter">
        <div data-aos="fade-up" data-aos-delay="500" class="row justify-content-center text-center items">
            <div data-percent="100" class="radial">
                <span></span>
            </div>
        </div>
    </div>

    <!-- Header -->
    <header id="header">

        <!-- Top Navbar -->
        <nav class="navbar navbar-expand top">
            <div class="container header">

                <!-- Nav holder -->
                <div class="ml-auto"></div>

                <!-- Navbar Items [right] -->
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-phone-alt mr-2"></i>+91 44 2434 2734</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-envelope mr-2"></i>admin@codeeng.net</a>
                    </li>
                </ul>

                <!-- Navbar Icons -->
                <ul class="navbar-nav icons">
                    <li class="nav-item social">
                        <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="nav-item social">
                        <a href="#" class="nav-link pr-0"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                </ul>

            </div>
        </nav>

        <!-- Navbar -->
        <nav class="navbar navbar-expand navbar-fixed sub">
            <div class="container header">

                <!-- Navbar Brand-->
                <a class="navbar-brand" href="/">
                    <img src="codewhite.png" alt="" style=" width: 160px; height: 46px; ">
                </a>

                <!-- Nav holder -->
                <div class="ml-auto"></div>

                <!-- Navbar Items -->
                <ul class="navbar-nav items">
                    <li class="nav-item">
                        <a href="#header" class="smooth-anchor nav-link">HOME</a>
                    </li>
                    <li class="nav-item">
                        <a href="#funfacts" class="smooth-anchor nav-link">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a href="#process" class="smooth-anchor nav-link">OUR TEAM</a>
                    </li>
                    <li class="nav-item">
                        <a href="#projects" class="smooth-anchor nav-link">OUR PROJECTS</a>
                    </li>
                </ul>

                <!-- Navbar Icons -->

                <!-- Navbar Toggle -->
                <ul class="navbar-nav toggle">
                    <li class="nav-item">
                        <a href="#" class="nav-link" data-toggle="modal" data-target="#menu">
                            <i class="icon-menu m-0"></i>
                        </a>
                    </li>
                </ul>

                <!-- Navbar Action -->
                <ul class="navbar-nav action">
                    <li class="nav-item ml-3">
                        <a href="#contact" class="smooth-anchor btn ml-lg-auto primary-button">CONTACT US</a>
                    </li>
                </ul>
            </div>
        </nav>

    </header>

    <!-- Hero -->
    <section id="slider" class="hero p-0 odd featured">
        <div class="swiper-container no-slider animation slider-h-50 slider-h-auto">
            <div class="swiper-wrapper">

                <!-- Item 1 -->
                <div class="swiper-slide slide-center">

                    <!-- Media -->
                    <img src="assets/images/about/cover.jpg" alt="Full Image" class="full-image" data-mask="80">

                    <div class="slide-content row text-center">
                        <div class="col-12 mx-auto inner">
                            <h1 class="mb-0 title effect-static-text">About Us</h1>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- About -->
    <section id="about" class="section-1 highlights team image-right">
        <div class="container">
            <div class="row pb-3">
                <div class="col-12 col-lg-12 align-self-top text">
                    <div class="row intro m-0">
                        <div class="col-12 p-0">
                            <h2><span class="featured"><span>Who  We Are</span></span></h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 p-0 pr-md-5">
                            <p>CODE has high quality capabilities in Civil and Interior as well as Engineering Services. Projects of Residential, Commercial, Institutional, Corporate, Hospitality and Industrial buildings.</p>
                            <p class="about-list"><i class="icon-arrow-right"></i>Experience of team over 20 years. Set to reach a 50 Cr turnover in 2020.</p>
                            <p class="about-list"><i class="icon-arrow-right"></i>Has a committed project order portfolio.</p>
                            <p class="about-list"><i class="icon-arrow-right"></i>Known for ethical techno commercial practices and timely completion</p>
                            <p>Turnkey capabilities and in-house expertise in manufacturing of product in a facility of a factory.</p>
                            <p>Our expertise spans an extensive range of services with Civil engineering excellence, design-build projects and construction management At Code Constructions the workforce is always driven to deliver projects promptly on-time using value driven solutions and top-notch project management techniques. We focus on supporting large-scale projects both private and public initiatives.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div data-aos="zoom-in" class="col-12 col-lg-12">
                    <div class="quote mt-5 mt-lg-0">
                        <ul class="list-group list-group-flush">

                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-eye" style="color:#474747!important;"></i></a>
                                <div class="list-group-content">
                                    <h4 class="abouttext">Vision</h4>
                                    <p>To be the world’s premier construction management organization – a company that our customers want to work with and our employees are proud to work for. Building and maintaining relationships with our customers is at the heart of our business. We aim to earn the lifetime loyalty of our customers by achieving extraordinary results by consistently delivering projects that meet international standards.</p>
                                </div>
                            </li>
                            <li class="list-group-item d-flex justify-content-center align-items-center">
                                <a href="#"><i class="icon icon-rocket" style="color:#474747!important;"></i></a>
                                <div class="list-group-content">
                                    <h4 class="abouttext">Mission</h4>
                                    <p>To be the pre-eminent provider of superior construction services by consistently improving the quality of our product and to maintain the highest level of professionalism, honesty and fairness in our relationships with our customers, employees and vendors.</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- About 3 -->
    <section id="about-3" class="section-3 highlights image-right featured">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>CONSTRUCTION ENGINEERING AND ADMINISTRATION</span></span></h2>
                            <div class="col-12 p-0 ">
                                <p class="about-list"><i class="icon-arrow-right"></i>Project management</p>
                                <p class="about-list"><i class="icon-arrow-right"></i>Construction bidding assistance</p>
                                <p class="about-list"><i class="icon-arrow-right"></i>Shop drawing reviews and approvals</p>
                                <p class="about-list"><i class="icon-arrow-right"></i>Design clarification</p>
                                <p class="about-list"><i class="icon-arrow-right"></i>Constructability reviews</p>
                                <p class="about-list"><i class="icon-arrow-right"></i>Cost estimates</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 p-0 image pr-3">
                    <img src="assets/images/about/01.jpg" class="fit-image" alt="Fit Image">
                </div>
            </div>
        </div>
    </section>
        <!-- About 3 -->
        <section id="about-3" class="section-4 highlights  featured">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-5 p-0 image">
                        <img src="assets/images/about/02.jpg" class="fit-image" alt="Fit Image">
                    </div>
                    <div class="col-12 col-md-7 pr-md-5 align-self-center text-center text-md-left text">
                        <div data-aos="fade-up" class="row intro">
                            <div class="col-12 p-0">
                                <h2 class="service-card"><span class="featured"><span>CONSTRUCTION OBSERVATION AND DOCUMENTATION</span></span></h2>
                                <div class="col-12 p-0 ">
                                    <p class="about-list"><i class="icon-arrow-right"></i>On-site observation during construction</p>
                                    <p class="about-list"><i class="icon-arrow-right"></i>Documentation of construction results relative to design plans and specifications</p>
                                    <p class="about-list"><i class="icon-arrow-right"></i>Evaluation of completed work for acceptance and payment</p>
                                    <p class="about-list"><i class="icon-arrow-right"></i>Record drawings</p>
                                    <p class="about-list"><i class="icon-arrow-right"></i>Job progress reporting</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    <!-- Testimonials -->
    <section id="testimonials" class="carousel testimonials">
        <div class="overflow-holder">
            <div class="container">
                <div class="row intro">
                    <div class="col-12">
                        <h2><span class="featured"><span>Technical Expertise</span></span>
                        </h2>
                        <p class="about-list"><i class="icon-arrow-right"></i>Construction Management <i class="icon-arrow-right"></i>Quantity and Quality Survey <i class="icon-arrow-right"></i>Cost and Variance Analysis </i>Bid Documentation and Contract Administration </p>
                        <p class="about-list"><i class="icon-arrow-right"></i>Bid Valuation and Negotiations <i class="icon-arrow-right"></i>Field Supervision Quality and Progress <i class="icon-arrow-right"></i>Project Documentation<i class="icon-arrow-right"></i>Field Survey and Feasibility </p>
                        <p  class="about-list"><i class="icon-arrow-right"></i>Project Advisory <i class="icon-arrow-right"></i>Master Planning <i class="icon-arrow-right"></i>Architectural Design </i>Master Planning <i class="icon-arrow-right"></i>Interior Design, Ergonomic Studies and Planning </p>
                        <p  class="about-list"><i class="icon-arrow-right"></i>Landscape Design and Environmental Impact Studies Services Design and Sustainability Ratings <i class="icon-arrow-right"></i>BIM Modelling and Technology Services</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about-3" class="section-4 highlights image-right featured">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro aos-init aos-animate">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>QUALITY POLICY</span></span></h2>
                            <div class="col-12 p-0 ">
                               <p>At-site deployment of Senior site supervisors for all processes, elaborate documentation and reporting, in-organization quality audits, scheduled management visits and periodic reviews, ensure quality workmanship, finishing, detailing and on-time completion of projects</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 p-0 image pr-3">
                    <img src="assets/images/about/qulitypol.jpg" class="fit-image" alt="Fit Image">
                </div>
            </div>
        </div>
    </section>
    <section id="about-3" class="section-3 highlights  featured">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5 p-0 image">
                    <img src="assets/images/about/ethicalpractices.jpg" class="fit-image fit-none" alt="Fit Image">
                </div>
                <div class="col-12 col-md-7 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro aos-init aos-animate">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>ETHICAL PRACTICES</span></span></h2>
                            <div class="col-12 p-0 ">
                                <p style="color:white">CODE CONSTRUCTIONS gives paramount importance to the highest ethical standards that is measured by the enduring quality of our projects. Having strong commitment towards safety, every CODE CONSTRUCTIONS project takes all required safety measures to warrant safe and sustainable working environments keeping in mind, guidelines from the Central and State regulatory agencies. At CODE CONSTRUCTIONS, we believe that the heart of any building is value engineering and we bring that to every project, in every city.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about-3" class="section-4 highlights image-right featured">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro aos-init aos-animate">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>TEAM </span></span></h2>
                            <div class="col-12 p-0 ">
                                <p>The team at Code has uncompromising standards of safety, quality, value engineering and timeline delivery. We constantly evolve our team of high-powered and motivated professionals with a wide range of expertise and training to skill up  for competence through testing initiatives. At CODE CONSTRUCTIONS, we believe that people are our greatest assets and the foundation for our growth and success. From engineers to surveyors, from finance executives to marketing professionals, we hand-pick only those who show passion, skills, integrity and commitment.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro aos-init aos-animate">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>STANDARDS AND TECHNOLOGY </span></span></h2>
                            <div class="col-12 p-0 ">
                                <p style=" padding-bottom: 143px; ">CODE CONSTRUCTIONS believes in constant innovation. We strive in setting new engineering standards in the field of construction and are continually researching and adopting new methods, technologies and practices to improve our skills with every project we undertake.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="about-3" class="section-3 highlights  featured">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12 pr-md-5 align-self-center text-center text-md-left text">
                    <div data-aos="fade-up" class="row intro aos-init aos-animate" style=" margin-bottom: 0px; ">
                        <div class="col-12 p-0">
                            <h2 class="service-card"><span class="featured"><span>EQUIPMENTS </span></span></h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 p-2 image">
                    <img src="assets/images/about/equipone.png" class="fit-image" alt="Fit Image">
                </div>
                <div class="col-12 col-md-6 p-2 image">
                    <img src="assets/images/about/equiptwo.png" class="fit-image" alt="Fit Image">
                </div>
            </div>
        </div>
    </section>
    <!-- Footer -->
<?php 
include ('footer-main.php');
?>